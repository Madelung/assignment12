package com.buildauto.mobiletesting;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;


public class MainActivityTest {
    public AppiumDriver driver;
    /**
     * Setup Appium
     */
    @Before
    public void setUp() throws Exception {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        //General
        capabilities.setCapability( "platformName" , "Android" );
        capabilities.setCapability( "commandTimeout" , 60 );
        capabilities.setCapability( "deviceReadyTimeout" , 20 );
        // Wearable
        capabilities.setCapability( "deviceName" , "emulator-5554" );
        capabilities.setCapability( "appPackage" , "com.buildauto.mobiletesting" );
        capabilities.setCapability( "appActivity" ,
                "com.buildauto.mobiletesting.MainActivity" );
        driver = new AndroidDriver( new URL( "http://127.0.0.1:4723/wd/hub" ), capabilities);
    }

    /**
     * Your tests
     */
    @Test
    public void test() throws IOException {

        //WaitExample
        WebDriverWait wait = new WebDriverWait(driver ,60 );
        wait.until(ExpectedConditions. presenceOfElementLocated(By.id( "speakButton" )));
        //ClickExample
        driver.findElement(By.id( "speakButton" )).click();
        //
        driver.findElement(By.id( "exampleElement" )).sendKeys( "Example Text" );
    }


    /**
     * Close Androiddriver
     */
    @After
    public void tearDown() throws Exception {
        if (driver != null) {
            driver.quit();
        }
    }
}