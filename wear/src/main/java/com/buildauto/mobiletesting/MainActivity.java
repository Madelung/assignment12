package com.buildauto.mobiletesting;

import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.ViewFlipper;


public class MainActivity extends WearableActivity implements AdapterView.OnItemSelectedListener {

    private static final int REQUEST_CODE = 1234;
    private Button speakButton;
    private Spinner messageSpinner;
    private ViewFlipper viewFlipper;
    private ImageView imageView;
    private static final SimpleDateFormat AMBIENT_DATE_FORMAT =
            new SimpleDateFormat("HH:mm", Locale.US);
    private String nodeId;
    private final int CONNECTION_TIME_OUT_MS = 2000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        speakButton = (Button) findViewById(R.id.speakButton);
        viewFlipper = (ViewFlipper) findViewById(R.id.myViewFlipper);
        messageSpinner = (Spinner) findViewById(R.id.messageSpinner);
        messageSpinner.setOnItemSelectedListener(this);
        imageView = (ImageView) findViewById(R.id.imageView);
        setAmbientEnabled();
    }

    public void speakButtonClicked(View view)
    {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.phrases, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        messageSpinner.setAdapter(adapter);
        viewFlipper.showNext();
    }

    public void sendMessage(View view)
    {
        String lValue = messageSpinner.getSelectedItem().toString().toUpperCase();

        if(lValue.contains("ERSTER ADVENT"))
        {
            imageView.setImageResource(R.mipmap.ic_advent01);
        }
        else if(lValue.contains("ZWEITER ADVENT"))
        {
            imageView.setImageResource(R.mipmap.ic_advent02);
        }
        else if(lValue.contains("DRITTER ADVENT"))
        {
            imageView.setImageResource(R.mipmap.ic_advent03);
        }
        else if(lValue.contains("VIERTER ADVENT"))
        {
            imageView.setImageResource(R.mipmap.ic_advent04);
        }

    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private String retrieveDeviceNode( final GoogleApiClient client) {
        client.blockingConnect(CONNECTION_TIME_OUT_MS, TimeUnit.MILLISECONDS);
        NodeApi.GetConnectedNodesResult result =
                Wearable.NodeApi.getConnectedNodes(client).await();
        List<Node> nodes = result.getNodes();
        if (nodes.size() > 0) {
            nodeId = nodes.get(0).getId();
        }
        return nodeId;
    }

    private void sendRequest( final String path, final String message ) {
        final GoogleApiClient client = new GoogleApiClient.Builder( this )
                .addApi(Wearable. API)
                .build();
        final String nodeId = retrieveDeviceNode(client);
        if (nodeId != null ) {
            new Thread( new Runnable() {
                @Override
                public void run() {
                    client .blockingConnect(CONNECTION_TIME_OUT_MS ,TimeUnit.MILLISECONDS);
                    Wearable.MessageApi.sendMessage(client ,nodeId ,path ,message.getBytes());
                    client.disconnect();
                }
            }).start();
        }
    }

}
