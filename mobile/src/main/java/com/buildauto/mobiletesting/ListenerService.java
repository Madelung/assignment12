package com.buildauto.mobiletesting;

import android.content.Intent;
import android.support.v4 .content.LocalBroadcastManager;
import android.widget.Toast;
import com.google.android .gms.wearable.MessageEvent;
import com.google.android .gms.wearable.WearableListenerService;

/**
 * Created by localadmin on 13.12.2016.
 */

public class ListenerService extends WearableListenerService {
    private LocalBroadcastManager broadcaster;

    static final public String LISTENER_RESULT =
            "com.buildauto.wearabletesting.ListenerService.REQUEST_PROCESSED" ;
    static final public String LISTENER_MESSAGE =
            "com.buildauto.wearabletesting.ListenerService.LISTENER_MSG" ;
    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        if ( messageEvent.getPath().equalsIgnoreCase( "/nachricht" ) ) {
            broadcaster = LocalBroadcastManager. getInstance( this );
            String message = new String(messageEvent.getData());
            Intent intent = new Intent(LISTENER_RESULT);
            if (message != null ) {
                intent.putExtra(LISTENER_MESSAGE, message);
            }
            broadcaster .sendBroadcast(intent);
        }
    }
}
