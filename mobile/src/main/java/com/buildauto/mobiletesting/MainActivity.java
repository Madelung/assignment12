package com.buildauto.mobiletesting;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView headline;
    ImageView advent;
    BroadcastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        headline = (TextView) findViewById(R.id.textView);
        advent = (ImageView) findViewById(R.id.imageView);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String str = intent.getStringExtra(ListenerService. LISTENER_MESSAGE).toUpperCase();
                if (str.contains( "ERSTER ADVENT" ))
                {
                    headline .setText( "Erster Advent" );
                    advent .setImageResource(R.mipmap. ic_advent01);
                }
                else if (str.contains( "ZWEITER ADVENT" ))
                {
                    headline .setText( "Zweiter Advent" );
                    advent .setImageResource(R.mipmap. ic_advent02);
                }
                else if (str.contains( "DRITTER ADVENT" ))
                {
                    headline .setText( "Dritter Advent" );
                    advent .setImageResource(R.mipmap. ic_advent03);
                }
                else if (str.contains( "VIERTER ADVENT" ))
                {
                    headline .setText( "Vierter Advent" );
                    advent .setImageResource(R.mipmap. ic_advent04);
                }
                else
                {
                    headline .setText(str);
                }
            }
        };
    }

    @Override
    protected void onStart() {
        super .onStart();
        LocalBroadcastManager. getInstance( this ).registerReceiver(( receiver ),
                new IntentFilter(ListenerService. LISTENER_RESULT)
        );
    }
    @Override
    protected void onStop() {
        LocalBroadcastManager. getInstance( this ).unregisterReceiver( receiver );
        super .onStop();
    }
}
